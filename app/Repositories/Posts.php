<?php

namespace app\Repositories;

use App\Post;
use App\Redis;

class Posts
{
    protected $redis;

    function __construct(Redis $redis)
    {
        $this->redis = $redis;
    }

    public function all()
    {
        return Post::all();
    }
}
