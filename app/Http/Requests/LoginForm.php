<?php

namespace App\Http\Requests;

// use App\User;
// use App\Mail\LoginMD;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Http\FormRequest;

class LoginForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required',
            'password' => 'required'
        ];
    }

    public function persist()
    {
        // Atempt to sign in
        auth()->attempt($this->only(['email', 'password']));

        // Posalji mail pri loginu
        // LoginMail
        // Mail::to(auth()->user())->send(new LoginMD);
    }
}
