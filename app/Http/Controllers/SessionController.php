<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use App\Http\Requests\LoginForm;

class SessionController extends Controller
{
    public function __constructor()
    {
        $this->middleware('guest', ['except' => 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('session.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LoginForm $form)
    {
        // Validate form preseljen u '/app/Http/Requests/Login.php'
        // Dobro je kod vecih formi, da izgleda cistije

        // Logika za login isto preseljena u '/app/Http/Requests/Login.php'
        // pod persist metodu
        $form->persist();
        // stari Atempt to sign in
        // if (! auth()->attempt(request(['email', 'password']))) {
        //     return back()->withErrors([
        //         'message' => 'Probajte ponovno'
        //     ]);
        // }

        // Messages spremamo u session
        // Prvi parametar key, drugi default vrijednost
        // session('message', 'Defaultna poruka');
        // Ako je prvi parametar array onda postavimo vrijednosti
        // session(['message' => 'Neka poruka']);

        // Flash session poruka znaci da je ziva samo tijekom trajanja jedne stranice/requesta, na refresh nestaje
        session()->flash('message', 'Dobrodosli');

        // If signind in, redirect
        return redirect()->home();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        auth()->logout();
        return redirect('login'); //->login();
    }
}
