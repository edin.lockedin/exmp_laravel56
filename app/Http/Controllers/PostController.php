<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        // U modelu scopeFilter
        $posts = Post::latest()
            ->filter([
                'month' => request('month'),
                'year' => request('year')
            ])
            ->get();

        // $archives = Post::archives();
        // $archives = Post::selectRaw('
        //     year(created_at) AS year,
        //     monthname(created_at) AS month,
        //     count(*) AS published
        // ')
        //     ->groupBy('year', 'month')
        //     ->orderByRaw('min(created_at) desc')
        //     ->get()
        //     ->toArray();


        return view('posts.index', compact('posts'));
        // return view('posts.index', [
        //     'posts' => $posts,
        //     'archives' => $archives
        // ]);
    }
    public function show(Post $post)
    {
        return view('posts.show', ['post' => $post]);
    }
    public function create()
    {
        return view('posts.create');
    }
    public function store()
    {
        $this->validate(request(), [
            'title' => 'required|max:40',
            'body' => 'required',
        ]);

        auth()->user()->publish(
            new Post(request(['title', 'body']))
        );
        // Post::create([
        //     'title' => request('title'),
        //     'body' => request('body'),
        //     'user_id' => auth()->id(),
        // ]);

        return redirect('/posts');
    }
}
