<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    // metoda eloquent modela. mozemo ju zvati iz routa
    // public static function complete()
    // {
        // static se valjda odnosi na cijelu klasu posto je ova metoda static
        // return static::where('completed', 1)->get();
    // }

    // pozivamo s `App\Task::complete()->get()` ili
    // `App\Task::complete()->where('id', '>=', 2)->get()`
    // Laravel prepoznaje 'scope' kao rezerviranu rijec?
    public function scopeComplete($query)
    {
        return $query->where('completed', 1);
    }
}
