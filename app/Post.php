<?php

namespace App;
use Carbon\Carbon;

class Post extends Model
{
    protected $fillable = ['title', 'body'];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function addComment($body)
    {
        $this->comments()->create([ 'body' => $body ]);
        // Comment::create([
        //     'body' => $body,
        //     'post_id' => $this->id
        // ]);
    }
    public function scopeFilter($query, $filters)
    {
        if ($month = $filters['month']) {
            $query->whereMonth('created_at', Carbon::parse($month)->month);
        }

        if ($year = $filters['year']) {
            $query->whereYear('created_at', $year);
        }
    }
    public static function archives()
    {
        return static::selectRaw('
            year(created_at) AS year,
            monthname(created_at) AS month,
            count(*) AS published
        ')
            ->groupBy('year', 'month')
            ->orderByRaw('min(created_at) desc')
            ->get()
            ->toArray();

    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
