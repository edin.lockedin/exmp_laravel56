<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Billing\Stripe;

class AppServiceProvider extends ServiceProvider
{
    // defer znaci ~odgodi, tj. nemoj pozivati dok nije potrebno
    // ako imamo nesto u boot metodi onda je ne mozemo deferati
    // protected $defer = true;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // boot metodu koristimo kada je laravel vec bootan
        // View composer service Provider
        // kad god neki view ukljucuje sidebar pridodaj mu archives iz Post klase
        view()->composer('layouts.sidebar', function ($view) {
            $archives = \App\Post::archives();
            $tags = \App\Tag::has('posts')->pluck('name');

            $view->with(compact(['archives', 'tags']));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // register koristimo da bi ukljucili stvari u service container
        // Service Container
        // nacin 1. \App::singleton('App\Billing\Stripe', function () {
        // singleton znaci koristi samo ovu instancu klase, ne registriraj nove
        // nacin 2. $this->app->singleton('App\Billing\Stripe', function () {
        // nacin 3. nakon sto smo na vrhu referencirali klasu sa `use`
        $this->app->singleton(Stripe::class, function () {
            // config from '/config/services'
            return new Stripe(config('services.stripe.secret'));
        });
        // callback funkcija prihvaca i parametar ako trebamo referencirati nesto drugo unutar nje
        // unutar '/config/app.php' je lista registriranih providera, redis kao primjer
    }
}
