@extends('layouts.master')

@section('content')
    <h2>{{ $post->title }}</h2>
    <p>
        <small>{{ $post->created_at->toFormattedDateString() }}</small>
        @if ($post->user)
            , <strong>{{ $post->user->name }}</strong>
        @endif
    </p>
    <p>{{ $post->body }}</p>
    <hr>
    <section>
        <ul class="list-group">Komentari:
            @foreach ($post->comments as $comment)
                <li class="list-group-item">
                    <p>
                        <small>{{ $comment->created_at->diffForHumans() }}</small>
                            {{-- {{ $comment->user->name }} --}}
                    </p>
                    <p>{{ $comment->body }}</p>
                </li>
            @endforeach
        </ul>
    </section>
    @if (count($post->tags))
        <section>
            <p><strong>Tags:</strong>
                @foreach ($post->tags as $tag)
                    <a href="/posts/tags/{{ $tag['name'] }}">
                        <span class="badge badge-secondary">{{ $tag['name'] }}</span>
                    </a>
                @endforeach
            </p>
        </section>
    @endif
    <section>
        <form class="" action="/posts/{{ $post->id }}/comments" method="POST">
            {{-- {{ method_field('PATCH') }} --}}
            {{ csrf_field() }}
            <div class="form-group">
                <label for="commnet-body">Add comment</label>
                <textarea class="form-control" id="commnet-body" name="body" required></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            @include('layouts.errors')
        </form>
    </section>
@endsection('content')
