@extends('layouts.master')

@section('content')
<h1>Publish a post</h1>
<form method="POST" action="/posts">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="post-title">Title</label>
        <input type="text" class="form-control" id="post-title" name="title" required>
    </div>
    <div class="form-group">
        <label for="post-body">Content</label>
        <textarea class="form-control" id="post-body" name="body" ></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    @include('layouts.errors')
</form>
@endsection('content')
