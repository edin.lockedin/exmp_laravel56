<div class="sidebar">
    Arhiva:
    <ul>
        @foreach ($archives as $month)
            <li><a href='/posts/?month={{ $month['month'] }}&year={{ $month['year'] }}'>
                {{ $month['month'] . ' ' . $month['year'] .' ('. $month['published'] . ')'}}
            </a></li>
        @endforeach
    </ul>
    Tagovi:
    <ul>
        @foreach ($tags as $tag)
            <li><a href='/posts/tags/{{ $tag }}'>
                {{ $tag }}
            </a></li>
        @endforeach
    </ul>
</div>
