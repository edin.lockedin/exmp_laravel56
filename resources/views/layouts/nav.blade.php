<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active"><a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a></li>
      <li class="nav-item"><a class="nav-link" href="/posts">Posts</a></li>
      <li class="nav-item"><a class="nav-link" href="/posts/create">New</a></li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
          <form class="" action="/logout" method="POST">
              @csrf
              <button type="submit" class="btn btn-link nav-link">Odjava</a>
          </form>
      </li>
      @if (Auth::check())
          <li class="nav-item ml-auto">
              <a class="nav-link" href="/users/{{ Auth::user()->id }}">Hi {{ Auth::user()->name }}</a>
          </li>
      @endif
    </ul>
  </div>
</nav>
