@if ($flash = session('message'))
    <div id="alert-container">
        <div class="alert alert-info" role="alert">
            {{ $flash }}
        </div>
    </div>
@endif
