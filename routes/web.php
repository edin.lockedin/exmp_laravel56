<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$stripe = resolve('App\Billing\Stripe');

Auth::routes();

Route::get('/login', 'SessionController@create')->name('login');
Route::post('/login', 'SessionController@store');
Route::post('/logout', 'SessionController@destroy');

Route::group(['middleware' => ['auth']], function () {
     //routes on which you want the middleware
     Route::get('/', 'HomeController@index')->name('home');

     Route::get('/tasks', 'TaskController@index');
     Route::get('/tasks/{task}', 'TaskController@show');

     Route::get('/posts', 'PostController@index');
     Route::get('/posts/create', 'PostController@create');
     Route::get('/posts/{post}', 'PostController@show');

     Route::get('/posts/tags/{tag}', 'TagController@index');

     Route::post('/posts', 'PostController@store');

     Route::post('/posts/{post}/comments', 'CommentController@store');
});

// GET     /posts
// GET     /posts/add
// POST    /posts
// GET     /posts/{id}
// GET     /posts/{id}/edit
// PATCH   /posts/{id}
// PUT     /posts/{id}
// DELETE  /posts/{id}
