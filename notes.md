# Laravel

## Model
- kreiramo (eloquent) model sa `php atrisan make:model User`
- unutar modela (/app) dodajemo metode, npr. `isActive`

stvori model i dodaj migraciju i controller
`php artisan make:model Post -mc`

## Controllers
```php
// laravel ce pretpostaviti da pretrazujemo po primary keyu
public function show(Task $id) {
    return view('tasks.show', ['task' => $task]);
}
public function show($id) {
    $task = Task::find($id);
    return view('tasks.show', ['task' => $task]);
}
```
